import 'package:flutter/material.dart';
import 'package:sharefare/login.dart';
//my imports
//import 'package:sharefare/login.dart';
import 'splashscreen.dart';
//import 'package:sharefare/searchride.dart';
//import 'package:sharefare/offerride.dart';
import 'intermediate.dart';
import 'package:provider/provider.dart';
import 'package:sharefare/provider/user_provider.dart';

var routes = <String, WidgetBuilder>{
  "/intro": (BuildContext context) => Intermediate(),
};

/* void main(){
  runApp(
    ChangeNotifierProvider(create: (_)=>UserProvider.initialize(),
  
  child:MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor:Colors.red.shade900
      ),
      home:SplashScreen(),
  )));
   
} */

class ScreenController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context);
    switch (user.status) {
      case Status.unintialized:
        return SplashScreen();
      case Status.unauthenticated:
      case Status.authenticating:
        return LoginPage();
      case Status.authenticated:
        return Intermediate();
      default:
        return LoginPage();
    }
  }
}
