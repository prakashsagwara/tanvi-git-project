import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sharefare/loading.dart';
import 'package:sharefare/offerride.dart';
import 'package:video_player/video_player.dart';
import "package:sharefare/loading.dart";
import 'dart:async';

class myData {
  String origin, destination, city,useruid;
String datetime;
  String noofseats;

  myData(this.origin, this.destination, this.city,this.datetime,this.noofseats,this.useruid);
}

class OfferedRide extends StatefulWidget  {
  @override
  _OfferedRideState createState() => _OfferedRideState();
}

class _OfferedRideState extends State<OfferedRide> {
  List<myData>allData=[];
  final  FirebaseAuth _firebase= FirebaseAuth.instance;
  FirebaseUser user;
  bool loading= true;

  getData()async{
    setState(() {
      loading=true;
    });

      FirebaseUser user = await _firebase.currentUser();
      print(user.uid.toString());
        DatabaseReference ref= FirebaseDatabase.instance.reference();
     ref.child('offerdride').orderByChild('userId').equalTo(user.uid).once().then((DataSnapshot snap){
     print('Data : ${snap.value}');
         var keys= snap.value.keys;
       var data= snap.value;
       allData.clear();

      for(var key in keys){
        myData d= new myData(data[key]["origin"],data[key]["destination"],data[key]["city"],data[key]["datetime"],data[key]["noofseats"],data[key]["useruid"]);
        allData.add(d);
      }
      setState(() {
      loading=false;
    });

    
    }
  );
      

     }
   
  
  @override
  void initState(){
    getData();
     
  }
    
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('offer-by-me'),
      ),
      
      body: ListView.builder(
                    itemCount: allData.length,
                    itemBuilder: (_, index) {
                      return UI(
                        allData[index].origin,
                        allData[index].destination,
                        allData[index].city,
                        allData[index].noofseats,
                       allData[index].datetime,
                      );
                    },
                  ),
              );
    
  }

  Widget UI(String origin, String destination, String city,String noofseats,String datetime) {
    return new Card(
      elevation: 10.0,
      child: new Container(
        padding: new EdgeInsets.all(20.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Text('Name : $origin',style: Theme.of(context).textTheme.title,),
            new Text('Profession : $destination'),
            new Text('noofseats : $noofseats'),
            new Text('city : $city'),
            new Text('date: $datetime'),
            
          ],
        ),
      ),
    );
  }
    
  

  }
  