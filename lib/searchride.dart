import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:datetime_picker_formfield/time_picker_formfield.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:sharefare/search.dart';
//import 'package:validate/validate.dart';  //for validation


class FindRide extends StatefulWidget {
  @override
  _FindRideState createState() => _FindRideState();
}

class _FindRideState extends State<FindRide> {

  int _currentStep = 0;
  String value;
  static MyData data = new MyData();
  static var _focusNode = new FocusNode();
  final _formKey = GlobalKey<FormState>();
  DateTime ddd;
  FirebaseAuth firebaseAuth= FirebaseAuth.instance;
  final dateFormat = DateFormat("yyyy-MM-dd");
  GetData search;
 
  TextEditingController origin= TextEditingController();
  TextEditingController destination= TextEditingController();
  String o;
  String d;
  String v;
  List<myData>allData=[];
  
  final timeFormat = DateFormat("h:mm");
  DateTime date;
  TimeOfDay time;

  @override
  Widget build(BuildContext context) {
           return new Scaffold(
             backgroundColor: Colors.white,
             resizeToAvoidBottomPadding: false, 
             appBar: AppBar(
              // backgroundColor: Colors.green,
               title: Text("FIND RIDE",
               style: TextStyle(color: Colors.white,),)
               ),
            body:  Form(
              key:_formKey,
                child: Stepper(
                type: StepperType.horizontal,
                currentStep: _currentStep,
                onStepTapped: (int step) => setState(() => _currentStep = step),
                onStepContinue:
                    _currentStep < 3 ? () => setState(() => _currentStep += 1) : null,
                onStepCancel:
                    _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
                controlsBuilder: (BuildContext context,{VoidCallback onStepContinue,VoidCallback onStepCancel}) {
                  return Row(
                    children: <Widget>[
                     _currentStep == 3 // this is the last step
                ? 
                  RaisedButton.icon(
                  icon: Icon(Icons.local_taxi),
                  onPressed: (){
                    searchrideform();
                  },
                  label: Text('Find Ride'),
                  color: Colors.cyan,
                )
                : RaisedButton.icon(
                  icon: Icon(Icons.navigate_next),
                  onPressed: onStepContinue,
                  label: Text('Next'),
                  color: Colors.grey,
                ),
                
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton.icon(
                    icon: Icon(Icons.delete_forever),
                    label: const Text('Back'),
                    color: Colors.blueGrey,
                    onPressed: onStepCancel,
                  ),
                )
                    ],
                  );
                },
                steps: <Step>[
                  new Step(
                    title: new Text('Location'),
                    content: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: origin,
                          //focusNode: _focusNode,
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onSaved: (String value) {
                            data.olocation = value;
                          },
                          maxLines: 1,
                          //initialValue: 'Aseem Wangoo',
                          validator: (value) {
                            if (value.isEmpty || value.length < 1) {
                              return 'Please enter origin';
                            }
                          },
                          decoration: InputDecoration(labelText: 'Origin'),
                        ),
                        TextFormField(
                          controller: destination,
                          focusNode: _focusNode,
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onSaved: (String value) {
                            data.dlocation = value;
                          },
                          maxLines: 1,
                          //initialValue: 'Aseem Wangoo',
                          validator: (value) {
                            if (value.isEmpty || value.length < 1) {
                              return 'Please enter destination';
                            }
                          },
                          decoration: InputDecoration(labelText: 'Destination'),
                        ),
                      ],
                    ),
                    isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 0 ? StepState.complete : StepState.disabled,
                  ),
                  new Step(
                    title: new Text('Date'),
                    content: Container(
                      height: 200,
                      width: 200,
                      child: ListView(
                        children: <Widget>[
                          Container(
                            child: DateTimePickerFormField(
                              format: dateFormat,
                              inputType: InputType.date,
                              decoration: InputDecoration(labelText: 'Date'),
                              onChanged: (dt) => setState(() => date = dt),
                            ),
                          ),
                        ],
                      ),
                    ),
                    isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 1 ? StepState.complete : StepState.disabled,
                  ),
                  new Step(
                    title: new Text('Time'),
                    content: Container(
                      height: 200,
                      width: 200,
                      child: ListView(
                        children: <Widget>[
                          Container(
                            child:TimePickerFormField(
                              format: timeFormat,
                              decoration: InputDecoration(labelText: 'Time'),
                              onChanged: (t) => setState(() => time = t),
                            ),
                          ),
                        ],
                      ),
                    ), 
                    isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 2 ? StepState.complete : StepState.disabled,
                  ),
                  new Step(
                    title: new Text('Seats'),
                    content: DropdownButton(
                  value: value,
              isExpanded: true,
              hint: Text("Choose no. of seats"),
              items: [
                DropdownMenuItem(
                  child: Text("1"),
                  value: "1",
                ),
                DropdownMenuItem(
                  child: Text("2"),
                  value: "2",
                ),
                DropdownMenuItem(
                  child: Text("3"),
                  value: "3",
                )
              ],
              onChanged: (newValue) {

                setState(() {
                    value = newValue;
                  });
              }),
                isActive:true,
                
              ),
          ],
        ),
            ),
      ); 
   
  }  
  dynamic myEncode(dynamic item) {
  if(item is DateTime) {
    return item.toIso8601String();
  }
  
   } 
Future searchrideform()async{
    FormState formState=_formKey.currentState;
    ddd= new DateTime(date.year,date.month,date.day,time.hour,time.minute);
    String dddd= myEncode(ddd);
    o= origin.text;
    d=destination.text;
    v=value.toString();
    
    if(formState.validate()){
      formState.reset();
      DatabaseReference ref= FirebaseDatabase.instance.reference();
     ref.child('offerdride').orderByChild('origin').equalTo(o).once().then((DataSnapshot snap){
     print('Data : ${snap.value}');
         var keys= snap.value.keys;
         var data= snap.value;
       allData.clear();

      for(var key in keys){
        if(d== data[key]["destination"]){
          print("filterdone".toString());
          print(dddd);
         
          if( dddd==data[key]["datetime"]){
            print("filterdone".toString());
            if(v==data[key]["noofseats"]){
              print("filterdone".toString());
               myData d= new myData(data[key]["origin"],data[key]["destination"],data[key]["city"],data[key]["datetime"],data[key]["noofseats"],data[key]["useruid"]);
               allData.add(d); 

            }
          }
          }
        }
        print(allData.length.toString());
    
    }
  );    
    
      Timer(Duration(seconds: 4), () => Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchedRide(allData))));
    }
  
  }
  
}
class MyData {
  String olocation = '';
  String dlocation = '';
}






