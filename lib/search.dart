import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sharefare/loading.dart';
import 'package:sharefare/offerride.dart';
import 'package:video_player/video_player.dart';
import "package:sharefare/loading.dart";
import 'dart:async';


class myData {
  String origin, destination, city,useruid;
String datetime;
  String noofseats;

  myData(this.origin, this.destination, this.city,this.datetime,this.noofseats,this.useruid);
}



class GetData{
   List<myData>allData=[];
  final  FirebaseAuth _firebase= FirebaseAuth.instance;
  FirebaseUser user;
  bool loading= true;

   getData(String origin , String destination,String datetime,String noofseats){
    
        DatabaseReference ref= FirebaseDatabase.instance.reference();
     ref.child('offerdride').orderByChild('origin').equalTo(origin).once().then((DataSnapshot snap){
     print('Data : ${snap.value}');
         var keys= snap.value.keys;
       var data= snap.value;
       allData.clear();
       print(origin.toString());
        print(destination.toString());
         print(datetime.toString());
          print(noofseats.toString());

      for(var key in keys){
        if(destination== data[key]["destination"]){
          print(data[key]["destination"].toString()); 
          if( datetime==data[key]["datetime"]){
            print(data[key]["datetime"].toString()); 
            if(noofseats==data[key]["noofseats"]){
              print(data[key]["noofseats"].toString());
               myData d= new myData(data[key]["origin"],data[key]["destination"],data[key]["city"],data[key]["datetime"],data[key]["noofseats"],data[key]["useruid"]);
               allData.add(d); 

            }
          }
          }
        }
        print(allData.length.toString());
        return allData;
    
    }
  );


     }
}
class SearchedRide extends StatefulWidget {
  final List<myData>alldata;
  const SearchedRide(this.alldata);
  
  @override
  _SearchedRideState createState() => _SearchedRideState(alldata);
}

class _SearchedRideState extends State<SearchedRide> {
  
  final List<myData>alldata;
 _SearchedRideState(this.alldata);
 @override
  void initState() {
    print(alldata.length.toString());
    super.initState();
  }
 
    
   @override
   
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Searched Ride'),
      ),
      
      body:alldata.length == 0
              ? 
                Loading()
              
              :Container(
                              child: ListView.builder(
                    itemCount: alldata.length,
                    itemBuilder: (_, index) {
                      return UI(
                        alldata[index].origin,
                        alldata[index].destination,
                        alldata[index].city,
                        alldata[index].noofseats,
                        alldata[index].datetime,
                      );
                    },
                  ),
              ));
    
  }

  Widget UI(String origin, String destination, String city,String noofseats,String datetime) {
    return new Card(
      elevation: 10.0,
      child: new Container(
        padding: new EdgeInsets.all(20.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Text('Name : $origin',style: Theme.of(context).textTheme.title,),
            new Text('Profession : $destination'),
            new Text('noofseats : $noofseats'),
            new Text('city : $city'),
            new Text('date: $datetime'),
            
          ],
        ),
      ),
    );
  }
    
}
