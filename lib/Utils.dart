import 'dart:io';
import 'dart:ui' as ui;

import 'package:downloader/downloader.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image/image.dart' as imageLib;
import 'package:intl/intl.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:share_extend/share_extend.dart';

class Utils {
  static double deviceWidth = 0.0;
  static double deviceHeight = 0.0;

  static const primaryColor = Color(0xFFED3269);
  static const white = Color(0xFFF7F8FA);
  static const grey = Color(0xFF7F8FA6);
  static const greyDark = Color(0xFF353B48);
  static const black = Color(0xFF20232A);
  static const blackLight = Color(0xFF2B2F3A);
  static const blue = Color(0xFF00A2FE);
  static const blue_dark = Color(0xFF00358D);
  static const inactive = Color(0xFF404040);
  static const gold = Color(0xFFffd700);

  // static const primaryAccentColor = Color(0xFFE10000);
  // static const primaryAccentColor = Color(0xFFF05F3E);
  // static const primaryAccentColor = Color(0xFF40D0E0);
  static const primaryAccentColor = Color(0xFF00326b);

  // static const primarySecondColor = Color(0xFF330099);
  // static const primarySecondColor = Color(0xFFED3269);
  // static const primarySecondColor = Color(0xFFDD00FF);
  // static const primarySecondColor = Color(0xFFc30081);
  static const primarySecondColor = Color(0xFFED3269);
  static const primaryDarkColor = Color(0xFFBD2854);
  static const erroColor = Color(0xFF7B8292);
  static const kGoogleApiKey = "AIzaSyBz7olg79TZH_4YgW941cQCOA_UHUeVcDs";

  static const beginKey = 0xfff683a4;
  static const endKey = 0xff0150a9;

  Gradient gradient() {
    return LinearGradient(
        colors: [Color(0xfff683a4), Color(0xff0150a9)],
        begin: FractionalOffset(1, 1),
        end: FractionalOffset(0, 0),
        stops: [0.0, 1.0],
        tileMode: TileMode.clamp);
  }

  var logger;
  String dateFormat({date}) {
    if (date == null) {
      return "";
    }
    return DateFormat.yMMMd().format(DateTime.parse(date)).toString();
  }

  String timeFormat({time}) {
    if (time == null) {
      return "";
    }
    return DateFormat.jm().format(DateTime.parse(time)).toString();
  }

  String time24to12Format({time}) {
    if (time == null) {
      return "";
    }
    return Utils().timeFormat(time: DateFormat("H:m:s").parse(time).toString());
  }

  void showToast(String tostmsg) {
    Fluttertoast.showToast(
        msg: tostmsg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.black54,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static shareNetworkFile(url, notes) async {
    Utils().showToast("Please wait. This might take a while");
    var file = await DefaultCacheManager().downloadFile(url);
    if (notes == null) {
      ShareExtend.share(file.file.path, "file");
    } else {
      await Share.file(
        'Eventos share',
        'Eventos.png',
        File(file.file.path).readAsBytesSync().buffer.asUint8List(),
        'image/png',
        text: notes,
      );
    }
  }

  static void saveNetworkImage(url) async {
    Downloader.getPermission();
    var path = url.split(".");
    int length = path.length;

    var time = "EventosHub" + DateTime.now().toString();

    if ((Platform.isAndroid))
      Downloader.download(url, "EventosHub" + time, ".${path[length - 1]}");
  }

  Future screenShot({@required globalKey, pixelRatio = 10}) async {
    try {
      // print('inside');
      RenderRepaintBoundary boundary =
          globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 10.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      var pngBytes = byteData.buffer.asUint8List();
      final directory = (await getApplicationDocumentsDirectory()).path;
      File imgFile = new File('$directory/screenshot.png');
      imgFile.writeAsBytes(pngBytes);

      // var bs64 = base64Encode(pngBytes);

      // setState(() {});
      /*  await Share.file(
        'Eventos share',
        'Eventos.png',
        pngBytes,
        'image/png',
      ); */
      // ShareExtend.share(bs64, "file");

      return imgFile;
    } catch (e) {
      print(e);
    }
  }

  void hideDialog() {}

  Future<void> someThingWentWrong({context, message}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/not_found.png'),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("$message"),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8, right: 8),
                  child: Row(
                    children: <Widget>[
                      Spacer(),
                      FlatButton(
                        child: Text(
                          "Ok",
                          style: TextStyle(color: Utils.primarySecondColor),
                        ),
                        onPressed: () => Navigator.pop(context),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> appCrashed({context}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/not_found.png'),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("We have recorded this event."),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8, right: 8),
                  child: Row(
                    children: <Widget>[
                      Spacer(),
                      FlatButton(
                        child: Text(
                          "Go to home",
                          style: TextStyle(color: Utils.primarySecondColor),
                        ),
                        onPressed: () => {},
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
