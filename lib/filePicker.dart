import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

import 'SizeConfig.dart';
import 'Utils.dart';

class PicturePicker extends StatefulWidget {
  final int id;
  final uploadMedium;
  final albums;

  const PicturePicker({Key key, this.id, this.uploadMedium, this.albums})
      : super(key: key);
  @override
  _PicturePickerState createState() => _PicturePickerState();
}

class _PicturePickerState extends State<PicturePicker> {
  String token;
  VideoPlayerController _controller;
  ChewieController _chewieController;
  bool isVideo = false;
  List<DropdownMenuItem> items = [];
  String selectedValue;
  TextEditingController descController = new TextEditingController();
  String fileName = "";
  File image;
  File videoFile;

  // final uploader = FlutterUploader();
  /* Future getImage() async {
    var img = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      image = img;
    });
  } */

  Future getImage() async {
    File imageLargeFile =
        await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageLargeFile == null) {
      return;
    }
    setState(() {
      image = imageLargeFile;
    });

    /// photo filter
    /*  var imagefile =
        await Utils().filterImage(image: imageLargeFile, context: context);
/*     var imageFile = await FlutterImageCompress.compressAndGetFile(
        imageLargeFile.path, imageLargeFile.path,
        quality: 80); */

    setState(() {
      image = imagefile;
    }); */
    /// for photo filter
    // var imagefile = await Utils().filterImage(image: img, context: context);

    /* var imageFile = await FlutterImageCompress.compressAndGetFile(
        img.path, img.path,
        quality: 80); */

    // print("image form gallery - $imageFile");
    /*  setState(() {
      image = imagefile;
    }); */
    /*  if (imagefile != null && imagefile.containsKey('image_filtered')) {
      setState(() {
        image = imagefile['image_filtered'];
      });
      // print(imageFile.path);
    } else {

    } */
  }

  Future getVideoCamera() async {
    var videoPath = await ImagePicker.pickVideo(source: ImageSource.camera);

    // print("video form gallery - $videoPath");
    setState(() {
      isVideo = true;
      videoFile = videoPath;
      if (videoFile != null) {
        _controller = VideoPlayerController.file(videoFile)
          ..initialize().then((_) {
            // _controller.addListener(listener);
            // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          });

        _chewieController = ChewieController(
          videoPlayerController: _controller,
          aspectRatio: _controller.value.aspectRatio,
          allowFullScreen: false,
        );
      } else {
        isVideo = false;
        setState(() {});
      }
    });
  }

  Future getVideo() async {
    var videoPath = await FilePicker.getFile(type: FileType.VIDEO);

    // print("video form gallery - $videoPath");
    setState(() {
      isVideo = true;
      videoFile = videoPath;
      if (videoFile != null) {
        _controller = VideoPlayerController.file(videoFile)
          ..initialize().then((_) {
            // _controller.addListener(listener);
            // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          });

        _chewieController = ChewieController(
          videoPlayerController: _controller,
          aspectRatio: 3 / 2,
          allowFullScreen: false,
        );
      } else {
        isVideo = false;
        setState(() {});
      }
    });
  }

  Future getCamera() async {
    File imageLargeFile =
        await ImagePicker.pickImage(source: ImageSource.camera);
    if (imageLargeFile == null) {
      return;
    }
    setState(() {
      image = imageLargeFile;
    });

    /// photo filter
    /*  var imagefile =
        await Utils().filterImage(image: imageLargeFile, context: context);
/*     var imageFile = await FlutterImageCompress.compressAndGetFile(
        imageLargeFile.path, imageLargeFile.path,
        quality: 80); */

    setState(() {
      image = imagefile;
    }); */
  }

  void _onIconPressed(context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              color: Color(0xFF737373),
              height: 250,
              child: Container(
                margin: EdgeInsets.only(left: 0, right: 0),
                child: _bottomNavigationMenu(),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(30),
                        topRight: const Radius.circular(30))),
              ));
        });
  }

  Column _bottomNavigationMenu() {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.camera, color: Utils.primaryAccentColor),
          title: Text(
            'Click Picture',
          ),
          onTap: () {
            SystemChannels.textInput.invokeMethod('TextInput.hide');

            Navigator.of(context).pop();
            getCamera();
          },
        ),
        ListTile(
            leading: Icon(Icons.image, color: Utils.primaryAccentColor),
            title: Text(
              'Select image from Phone Gallery',
            ),
            onTap: () {
              SystemChannels.textInput.invokeMethod('TextInput.hide');

              Navigator.of(context).pop();
              getImage();
            }),
        ListTile(
            leading: Icon(Icons.videocam, color: Utils.primaryAccentColor),
            title: Text(
              'Select video from Phone Gallery',
            ),
            onTap: () {
              SystemChannels.textInput.invokeMethod('TextInput.hide');

              Navigator.of(context).pop();
              getVideo();
            }),
        ListTile(
            leading: Icon(Icons.videocam, color: Utils.primaryAccentColor),
            title: Text(
              'Record Video',
            ),
            onTap: () {
              SystemChannels.textInput.invokeMethod('TextInput.hide');

              Navigator.of(context).pop();
              getVideoCamera();
            }),

        /* ListTile(
          leading: Icon(Icons.video_library,color: Color(0xff3E8DF3)),
          title: Text('Video',style: TextStyle(color: Colors.black)),
          onTap: () {},
        ),*/
        /* Row(
          children: <Widget>[
            Spacer(),
            FlatButton(
              child: Text(
                'Cancel',
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ) */
      ],
    );
  }

  @override
  void initState() {
    for (int i = 0; i < 10; i++) {
      items.add(new DropdownMenuItem(
        child: new Text(
          'Album ' + i.toString(),
        ),
        value: 'Album ' + i.toString(),
      ));
    }

    if (widget.uploadMedium == "Image-Camera") getCamera();
    if (widget.uploadMedium == "Image-Gallery") getImage();
    if (widget.uploadMedium == "Video-Camera") getVideoCamera();
    if (widget.uploadMedium == "Video-Gallery") getVideo();
    super.initState();
  }

  /*  Future backgroundUpload(String gallery_album_id, String notes) async {
    final taskId = await uploader.enqueue(
        url: baseUrl +
            "/user/events/gallery/items/add", //required: url to upload to
        files: [
          FileItem(
              filename: path.basename(image.path),
              savedDir: path.dirname(image.path),
              fieldname: "media_file")
        ], // required: list of files that you want to upload
        method: UploadMethod.POST, // HTTP method  (POST or PUT or PATCH)
        headers: headers,
        data: {
          "gallery_album_id": gallery_album_id,
          "notes": notes,
        }, // any data you want to send in upload request
        showNotification:
            true, // send local notification (android only) for upload status
        tag: "upload 1"); // unique tag for upload task
  }

  Future backgroundUploadVideo(String gallery_album_id, String notes) async {
    final taskId = await uploader.enqueue(
        url: baseUrl +
            "/user/events/gallery/items/add", //required: url to upload to
        files: [
          FileItem(
              filename: path.basename(videoFile.path),
              savedDir: path.dirname(videoFile.path),
              fieldname: "media_file")
        ], // required: list of files that you want to upload
        method: UploadMethod.POST, // HTTP method  (POST or PUT or PATCH)
        headers: headers,
        data: {
          "gallery_album_id": gallery_album_id,
          "notes": notes,
          "media_type": "Video",
        }, // any data you want to send in upload request
        showNotification:
            true, // send local notification (android only) for upload status
        tag: "upload 1"); // unique tag for upload task
  } */

  @override
  void dispose() {
    super.dispose();
    if (_controller != null) {
      _controller.dispose();
      _chewieController.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: GradientAppBar(
        backgroundColorStart: Utils.primaryAccentColor,
        backgroundColorEnd: Utils.primarySecondColor,
        centerTitle: true,
        title: Text(
          'Add to Album',
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: InkWell(
                onTap: () {
                  SystemChannels.textInput.invokeMethod('TextInput.hide');

                  if (_controller != null) {
                    if (_controller.value.isPlaying) {
                      _controller.pause();
                    } else {
                      _controller.play();
                    }
                  } else {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');

                    _onIconPressed(context);
                  }
                },
                /* getImage */
                child: Container(
                  height: SizeConfig.safeBlockVertical * 30,
                  width: SizeConfig.screenWidth,
                  child: image == null && !isVideo
                      ? DottedBorder(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Center(
                                child: Image.asset(
                                  'assets/pictures.png',
                                  fit: BoxFit.cover,
                                  height: SizeConfig.safeBlockVertical * 10,
                                ),
                              ),
                              Text(
                                'Click to upload',
                              ),
                            ],
                          ),
                        )
                      : isVideo
                          ? Center(
                              child: _chewieController != null
                                  ? Chewie(
                                      controller: _chewieController,
                                    )
                                  : Container(),
                            )
                          : Image.file(image, fit: BoxFit.cover),
                ),
              ),
            ),
            ListBody(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(5),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      controller: descController,
                      textCapitalization: TextCapitalization.sentences,
                      maxLines: 5,
                      onChanged: (value) {},
                      cursorColor: Colors.black,
                      autofocus: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5),
                      decoration: InputDecoration(
                          fillColor: Colors.transparent,
                          labelText: "Description",
                          filled: true,
                          hintText: "Description...",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                style: BorderStyle.none,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)))),
                    ),
                  ),
                ),
              ],
            ),
            Visibility(
              visible: false,
              child: Padding(
                padding:
                    EdgeInsets.only(top: 30, bottom: 30, left: 50, right: 50),
                child: InkWell(
                  onTap: () async {},
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      gradient: LinearGradient(
                          colors: [
                            Utils.primaryAccentColor,
                            Utils.primarySecondColor,
                          ],
                          begin: const FractionalOffset(0.1, 0.5),
                          end: const FractionalOffset(1.0, 1.0),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 10, left: 20, right: 20, bottom: 10),
                      child: Text(
                        "Add",
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class VideoPicker extends StatefulWidget {
  @override
  _VideoPickerState createState() => _VideoPickerState();
}

class _VideoPickerState extends State<VideoPicker> {
  List<DropdownMenuItem> items = [];
  String selectedValue;
  String _path;
  Map<String, String> _paths;
  String _extension = "mp4";
  @override
  void initState() {
    for (int i = 0; i < 10; i++) {
      items.add(new DropdownMenuItem(
        child: new Text(
          'Album ' + i.toString(),
        ),
        value: 'Album ' + i.toString(),
      ));
    }
    super.initState();
  }

  File imageFile;
  File videoFile;
  File thumbnailFile;
  bool isLoading;
  bool isShowSticker;
  String imageUrl;
  String videoUrl;
  String thumbnailUrl;
  String fileName;
  bool loadingPath = false;

  Future getVideo() async {
    if (FileType.VIDEO != FileType.CUSTOM) {
      setState(() => loadingPath = true);
      try {
        _paths = null;
        _path = await FilePicker.getFilePath(type: FileType.VIDEO);

        // _getThumb(_path);
      } catch (e) {}
      if (!mounted) return;
      setState(() {
        loadingPath = false;
        fileName = _path != null
            ? _path.split('/').last
            : _paths != null ? _paths.keys.toString() : '...';
      });

      videoFile = File(_path);
    }
  }

  /* Future<String> _getThumb(String pathThumb) async {
    var appDocDir = await getApplicationDocumentsDirectory();
    final folderPath = appDocDir.path;
    String thumbnail = await Thumbnails.getThumbnail(
        thumbnailFolder: folderPath,
        videoFile: pathThumb,
        imageType: ThumbFormat.PNG,
        quality: 30);
    thumbnailFile = File(thumbnail);

    if (videoFile != null && thumbnailFile != null) {
      setState(() {
        isLoading = true;
      });
    }
    return thumbnail;
  } */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        backgroundColorStart: Utils.primaryAccentColor,
        centerTitle: true,
        backgroundColorEnd: Utils.primarySecondColor,
        title: const Text(
          'Add to Album',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: InkWell(
                // onTap: getVideo,
                onTap: () {},
                child: Container(
                    height: SizeConfig.safeBlockVertical * 30,
                    // width: SizeConfig.safeBlockHorizontal * 4,
                    child: thumbnailFile == null
                        ? DottedBorder(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Center(
                                  child: Image.asset(
                                    'assets/pictures.png',
                                    fit: BoxFit.cover,
                                    height: SizeConfig.safeBlockVertical * 10,
                                  ),
                                ),
                                Text(videoFile == null
                                    ? 'Click to upload'
                                    : 'Video Selected'),
                              ],
                            ),
                          )
                        : Container(
                            height: SizeConfig.safeBlockVertical * 30,
                            child: Image.file(thumbnailFile),
                          )),
              ),
            ),
            ListBody(
              children: <Widget>[
                /*     Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 25, top: 15),
                      width: SizeConfig.screenWidth - 28,
                      child: SearchableDropdown(
                        isExpanded: true,
                        items: items,
                        value: selectedValue,
                        hint: Text('Select Album'),
                        searchHint: Center(
                          child: Text(
                            'Select Album',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                        onChanged: (value) {
                          setState(() {
                            selectedValue = value;
                          });
                        },
                      ),
                    ),
                  ],
                ), */
                Padding(
                  padding: EdgeInsets.all(14),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      maxLines: 5,
                      onChanged: (value) {},
                      cursorColor: Colors.black,
                      autofocus: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5),
                      decoration: InputDecoration(
                          fillColor: Colors.transparent,
                          labelText: "Description",
                          filled: true,
                          labelStyle: TextStyle(color: Colors.black),
                          hintText: "Description...",
                          hintStyle: TextStyle(
                              color: Colors.black,
                              fontSize: SizeConfig.safeBlockHorizontal * 3.2),
                          prefixIcon: Icon(
                            Icons.description,
                            color: Colors.black,
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                style: BorderStyle.none,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)))),
                    ),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                alignment: Alignment.center,
                margin:
                    EdgeInsets.only(top: 30, bottom: 30, left: 50, right: 50),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  gradient: LinearGradient(
                      colors: [
                        Utils.primaryAccentColor,
                        Utils.primarySecondColor,
                      ],
                      begin: const FractionalOffset(0.1, 0.5),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                  child: Text(
                    "Add Video",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AlbumPicker extends StatefulWidget {
  @override
  _VideoAlbumPickerState createState() => _VideoAlbumPickerState();
}

class _VideoAlbumPickerState extends State<AlbumPicker> {
  String token;
  void initState() {
    super.initState();
  }

  TextEditingController titleController = new TextEditingController();
  TextEditingController descController = new TextEditingController();
  File image;

  Future getImage() async {
    var img = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      image = img;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      /*   bottomNavigationBar: Container(
        height: 60,
        child: RaisedButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'Add to Album',
            style: TextStyle(color: Colors.white),
          ),
          color: Utils.primaryAccentColor,
        ),
      ), */
      appBar: GradientAppBar(
        backgroundColorStart: Utils.primaryAccentColor,
        backgroundColorEnd: Utils.primarySecondColor,
        centerTitle: true,
        title: const Text(
          'Create Album',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Visibility(
              visible: false,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: InkWell(
                  onTap: getImage,
                  child: Container(
                    height: SizeConfig.safeBlockVertical * 30,
                    // width: SizeConfig.safeBlockHorizontal * 4,
                    child: image == null
                        ? DottedBorder(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Center(
                                  child: Image.asset(
                                    'assets/pictures.png',
                                    fit: BoxFit.cover,
                                    height: SizeConfig.safeBlockVertical * 10,
                                  ),
                                ),
                                Text('Click to upload'),
                              ],
                            ),
                          )
                        : Image.file(image, fit: BoxFit.cover),
                  ),
                ),
              ),
            ),
            ListBody(
              children: <Widget>[
                /*  Padding(
                  padding: EdgeInsets.all(14),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      onChanged: (value) {},
                      cursorColor: Colors.black,
                      autofocus: false,
                      controller: titleController,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5),
                      decoration: InputDecoration(
                          fillColor: Colors.transparent,
                          labelText: "Title",
                          filled: true,
                          labelStyle: TextStyle(color: Colors.black),
                          hintText: "Title...",
                          hintStyle: TextStyle(
                              color: Colors.black,
                              fontSize: SizeConfig.safeBlockHorizontal * 3.2),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                style: BorderStyle.none,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)))),
                    ),
                  ),
                ), */
                Container(
                  margin: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 20,
                  ),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.title,
                        size: 15,
                        color: Utils.primaryColor,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: 8.0,
                        ),
                        child: Text(
                          'Title',
                          style: TextStyle(
                            color: Utils.primaryColor,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: 20,
                    right: 20, /*  top: 20, bottom: 10 */
                  ),
                  child: TextField(
                    textCapitalization: TextCapitalization.words,
                    controller: titleController,
                    /*        onSubmitted: submitTitle, */
                    cursorColor: Colors.black,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black45),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Utils.primarySecondColor),
                      ),
                      contentPadding: EdgeInsets.only(top: 8, bottom: 3),
                      hintText: '',
                      hintStyle: TextStyle(
                        color: Colors.black45,
                      ),
                    ),
                    style: TextStyle(
                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                InkWell(
                  onTap: () {},
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          top: 20,
                        ),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.title,
                              size: 15,
                              color: Utils.primaryColor,
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                left: 8.0,
                              ),
                              child: Text(
                                'Associate with sub-event',
                                style: TextStyle(
                                  color: Utils.primaryColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize:
                                      SizeConfig.safeBlockHorizontal * 3.5,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20, /*  top: 20, bottom: 10 */
                        ),
                        child: TextField(
                          enabled: false,

                          // controller: titleController,
                          /*        onSubmitted: submitTitle, */
                          cursorColor: Colors.black,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black45),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Utils.primarySecondColor),
                            ),
                            contentPadding: EdgeInsets.only(top: 8, bottom: 3),
                            hintText: '',
                            hintStyle: TextStyle(
                              color: Colors.black45,
                            ),
                          ),
                          style: TextStyle(
                              fontSize: SizeConfig.safeBlockHorizontal * 4,
                              color: Colors.black),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          top: 20,
                        ),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.title,
                              size: 15,
                              color: Utils.primaryColor,
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                left: 8.0,
                              ),
                              child: Text(
                                'Set guest list visibility',
                                style: TextStyle(
                                  color: Utils.primaryColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize:
                                      SizeConfig.safeBlockHorizontal * 3.5,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20, /*  top: 20, bottom: 10 */
                        ),
                        child: TextField(
                          enabled: false,
                          // controller: titleController,
                          /*        onSubmitted: submitTitle, */
                          cursorColor: Colors.black,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black45),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Utils.primarySecondColor),
                            ),
                            contentPadding: EdgeInsets.only(top: 8, bottom: 3),
                            hintText: '',
                            hintStyle: TextStyle(
                              color: Colors.black45,
                            ),
                          ),
                          style: TextStyle(
                              fontSize: SizeConfig.safeBlockHorizontal * 4,
                              color: Colors.black),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                    ],
                  ),
                ),

                /*  Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(5),
                  margin:
                      EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      border: Border.all(color: Colors.black45)
                      /*   gradient: LinearGradient(
                        colors: [
                          Utils.primaryAccentColor,
                          // Utils.primaryAccentColor,
                          Utils.primarySecondColor,
                          // Utils.primarySecondColor,
                        ],
                        begin: const FractionalOffset(0.1, 0.5),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp), */
                      ),
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: 10, left: 20, right: 20, bottom: 10),
                    child: Text(
                      "Associate with sub-event",
                      style: TextStyle(
                        color: Colors.black,
                        // fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                      ),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(5),
                  margin:
                      EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      border: Border.all(color: Colors.black45)
                      /*  gradient: LinearGradient(
                        colors: [
                          Utils.primaryAccentColor,
                          // Utils.primaryAccentColor,
                          Utils.primarySecondColor,
                          // Utils.primarySecondColor,
                        ],
                        begin: const FractionalOffset(0.1, 0.5),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp), */
                      ),
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: 10, left: 20, right: 20, bottom: 10),
                    child: Text(
                      "Set guest list visibility",
                      style: TextStyle(
                        color: Colors.black,
                        // fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                      ),
                    ),
                  ),
                ), */
                Visibility(
                  visible: false,
                  child: Padding(
                    padding: EdgeInsets.all(14),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        maxLines: 5,
                        onChanged: (value) {},
                        cursorColor: Colors.black,
                        autofocus: false,
                        controller: descController,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: SizeConfig.safeBlockHorizontal * 3.5),
                        decoration: InputDecoration(
                            fillColor: Colors.transparent,
                            labelText: "Description",
                            filled: true,
                            labelStyle: TextStyle(color: Colors.black),
                            hintText: "Description...",
                            hintStyle: TextStyle(
                                color: Colors.black,
                                fontSize: SizeConfig.safeBlockHorizontal * 3.2),
                            prefixIcon: Icon(
                              Icons.description,
                              color: Colors.black,
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  width: 1,
                                  style: BorderStyle.none,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)))),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Visibility(
              visible: titleController.text.length == 0,
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 30, bottom: 30, left: 50, right: 50),
                child: InkWell(
                  onTap: () async {
                    if (titleController.text.length == 0) {
                      Utils().showToast("Please enter album title.");
                    }
                    /*   Navigator.of(context).pushAndRemoveUntil(
                        new MaterialPageRoute(builder: (BuildContext context) {
                      return GalleryMgmt(
                        isVisible: true,
                      );
                    }), (Route<dynamic> route) => true); */
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      // borderRadius: BorderRadius.all(Radius.circular(30.0)),
                      gradient: LinearGradient(
                          colors: [
                            Colors.grey,
                            Colors.grey,
                          ],
                          begin: const FractionalOffset(0.1, 0.5),
                          end: const FractionalOffset(1.0, 1.0),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 10, left: 20, right: 20, bottom: 10),
                      child: Text(
                        "Create",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: titleController.text.length != 0,
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 30, bottom: 30, left: 50, right: 50),
                child: InkWell(
                  onTap: () async {
                    if (titleController.text.length > 0) {
                    } else {
                      Utils().showToast("Please enter album title.");
                    }
                    /*   Navigator.of(context).pushAndRemoveUntil(
                        new MaterialPageRoute(builder: (BuildContext context) {
                      return GalleryMgmt(
                        isVisible: true,
                      );
                    }), (Route<dynamic> route) => true); */
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      // borderRadius: BorderRadius.all(Radius.circular(30.0)),
                      gradient: LinearGradient(
                          colors: [
                            Utils.primaryAccentColor,
                            Utils.primarySecondColor,
                          ],
                          begin: const FractionalOffset(0.1, 0.5),
                          end: const FractionalOffset(1.0, 1.0),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 10, left: 20, right: 20, bottom: 10),
                      child: Text(
                        "Create",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.safeBlockHorizontal * 4.5,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
/* class ImageFilePicker extends StatefulWidget {
   @override
   _ImageFilePickerState createState() => _ImageFilePickerState();
 }
 
 class _ImageFilePickerState extends State<ImageFilePicker> {
  File image;

  Future getImage() async {
    var img = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      image = img;
    });
  }

  

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      /*   bottomNavigationBar: Container(
        height: 60,
        child: RaisedButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'Add to Album',
            style: TextStyle(color: Colors.white),
          ),
          color: Utils.primaryAccentColor,
        ),
      ), */
      appBar: GradientAppBar(
        backgroundColorStart: Utils.primaryAccentColor,
        backgroundColorEnd: Utils.primarySecondColor,
        centerTitle: true,
        title: const Text(
          'Create Album',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: InkWell(
                onTap: getImage,
                child: Container(
                  height: SizeConfig.safeBlockVertical * 30,
                  // width: SizeConfig.safeBlockHorizontal * 4,
                  child: image == null
                      ? DottedBorder(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Center(
                                child: Image.asset(
                                  'assets/pictures.png',
                                  fit: BoxFit.cover,
                                  height: SizeConfig.safeBlockVertical * 10,
                                ),
                              ),
                              Text('Click to upload'),
                            ],
                          ),
                        )
                      : Image.file(image, fit: BoxFit.cover),
                ),
              ),
            ),
            ListBody(
              children: <Widget>[

                Padding(
                  padding: EdgeInsets.all(14),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      onChanged: (value) {},
                      cursorColor: Colors.black,
                      autofocus: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5),
                      decoration: InputDecoration(
                          fillColor: Colors.transparent,
                          labelText: "Title",
                          filled: true,
                          labelStyle: TextStyle(color: Colors.black),
                          hintText: "Title...",
                          hintStyle: TextStyle(
                              color: Colors.black,
                              fontSize: SizeConfig.safeBlockHorizontal * 3.2),
                         
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                style: BorderStyle.none,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)))),
                    ),
                  ),
                ),
              
               Padding(
                  padding: EdgeInsets.all(14),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      maxLines: 5,
                      onChanged: (value) {},
                      cursorColor: Colors.black,
                      autofocus: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig.safeBlockHorizontal * 3.5),
                      decoration: InputDecoration(
                          fillColor: Colors.transparent,
                          labelText: "Description",
                          filled: true,
                          labelStyle: TextStyle(color: Colors.black),
                          hintText: "Description...",
                          hintStyle: TextStyle(
                              color: Colors.black,
                              fontSize: SizeConfig.safeBlockHorizontal * 3.2),
                          prefixIcon: Icon(
                            Icons.description,
                            color: Colors.black,
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                width: 1,
                                style: BorderStyle.none,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)))),
                    ),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                alignment: Alignment.center,
                margin:
                    EdgeInsets.only(top: 30, bottom: 30, left: 50, right: 50),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  gradient: LinearGradient(
                      colors: [
                        Utils.primaryAccentColor,
                        Utils.primarySecondColor,
                      ],
                      begin: const FractionalOffset(0.1, 0.5),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                  child: Text(
                    "Create Album",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.safeBlockHorizontal * 4,),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
 }
 */
