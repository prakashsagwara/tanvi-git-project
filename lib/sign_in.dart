/* import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sharefare/intermediate.dart';
import 'home.dart';
import 'loading.dart';
import 'main.dart';
import 'package:provider/provider.dart';
import 'package:sharefare/provider/user_provider.dart';
import 'login.dart';
import './db/users.dart';

class SigninPage extends StatefulWidget {

  @override
  _SigninPageState createState() => _SigninPageState();
}
class _SigninPageState extends State<SigninPage> {
  final _formKeys= GlobalKey<FormState>();
  final _key= GlobalKey<ScaffoldState>();
  final FirebaseAuth firebaseAuth=FirebaseAuth.instance;
  UserServices _userServices= UserServices();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _confirmTextController = TextEditingController();
  String gender;
  String groupValue="male" ;
  bool loading=false;
  bool hidepass=true;
  
  @override
  Widget build(BuildContext context) {
    final user= Provider.of<UserProvider>(context);
    return Scaffold(
      key: _key,
      resizeToAvoidBottomPadding: false,
      body: user.status== Status.authenticating?Loading():Stack(
            children: <Widget>[
              Image.asset('images/back.jpg',
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,),
              Container(
                color:Colors.black.withOpacity(0.5),
                height:double.infinity ,
                width:double.infinity,),
                Padding(
                  padding:const EdgeInsets.only(top:200.0) ,
                      child: Center(
                      child:Form(
                        key: _formKeys,
                        child:
                       Column(children: <Widget>[
  
                         Padding(
                           padding: EdgeInsets.fromLTRB(12.0,8.0,12.0,8.0),
                                                  child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white.withOpacity(0.7),
                            elevation: 0.0,
                            child:Padding(
                              padding: const EdgeInsets.only(left:12.0),
                              child: TextFormField(
                               controller: _nameTextController,
                               decoration: InputDecoration(
                                    hintText: "Name",
                                    icon: Icon(Icons.alternate_email),
                                    
                                ),
                              validator: (value) {
                                        if (value.isEmpty) {
                                         return "enter your name first";
                                         
                                        
                                        }
                                            return null;
                                        
                                      },
                                    ),
                            )
                           ),
                         ), 

                         Padding(
                           padding:EdgeInsets.fromLTRB(12.0,8.0,12.0,8.0) ,
                                                    child: Container(
                             color: Colors.white.withOpacity(0.4),
                             child: Row(
                               children: <Widget>[
                                 Expanded(child: ListTile(title:Text("male",style:TextStyle(color: Colors.white)), trailing:Radio(value: "male", groupValue: groupValue, onChanged: (e)=>ValueChanged(e)))),
                                 Expanded(child: ListTile(title:Text("female",style:TextStyle(color: Colors.white)), trailing:Radio(value: "female", groupValue: groupValue, onChanged: (e)=>ValueChanged(e))))
                               ],

                             ),
                           ),
                         ),
                         Padding(
                           padding: EdgeInsets.fromLTRB(12.0,8.0,12.0,8.0),
                                                  child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white.withOpacity(0.7),
                            elevation: 0.0,
                            child:Padding(
                              padding: const EdgeInsets.only(left:12.0),
                              child: TextFormField(
                               controller: email,
                               decoration: InputDecoration(
                                    hintText: "Email",
                                    icon: Icon(Icons.alternate_email),
                                    
                                ),
                              validator: (value) {
                                        if (value.isEmpty) {
                                          Pattern pattern =
                                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                          RegExp regex = new RegExp(pattern);
                                          if (!regex.hasMatch(value))
                                            return 'Please make sure your email address is valid';
                                          else
                                            return null;
                                        }
                                      },
                                    ),
                            )
                           ),
                         ),
                        
                         
                         Padding(
                           padding: EdgeInsets.fromLTRB(12.0,8.0,12.0,8.0),
                                                  child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white.withOpacity(0.7),
                            elevation: 0.0,
                            child:Padding(
                              padding: const EdgeInsets.only(left:12.0),
                              child: ListTile(
                                 title: TextFormField(
                                 controller: password,
                                 obscureText: hidepass,
                                 decoration: InputDecoration(
                                      hintText: " Password",
                                      
                                      icon: Icon(Icons.lock_outline),
                                      
                                  ),
                                validator: (value) {
                                          if (value.isEmpty) {
                                            return "the password field cannot be empty";
                                          }
                                          else if(value.length<6){
                                            return "the password length should be greater then 6";
                                          }
                                          return null;
                                        },
                                      ),
                                      trailing: IconButton(icon: Icon(Icons.remove_red_eye), onPressed: (){setState(() {
                                        hidepass=false;
                                      });}),
                              ),
                            )
                           ),
                         ),
                         Padding(
                           padding: EdgeInsets.fromLTRB(12.0,8.0,12.0,8.0),
                                                  child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white.withOpacity(0.7),
                            elevation: 0.0,
                            child:Padding(
                              padding: const EdgeInsets.only(left:12.0),
                              child: ListTile(
                                 title: TextFormField(
                                 controller: _confirmTextController,
                                 obscureText: hidepass,
                                 decoration: InputDecoration(
                                      hintText: " ConfirmPassword",
                                      
                                      icon: Icon(Icons.lock_outline),
                                      
                                  ),
                                validator: (value) {
                                          if (value.isEmpty) {
                                            return "the password field cannot be empty";
                                          }
                                          else if(value.length<6){
                                            return "the password length should be greater then 6";
                                          }
                                          return null;
                                        },
                                      ),
                                      trailing: IconButton(icon: Icon(Icons.remove_red_eye), onPressed: (){
                                        setState(() {
                                          hidepass=false;
                                        });
                                      }),
                              ),
                            )
                           ),
                         ),
                          Padding(
                           padding: EdgeInsets.fromLTRB(12.0,8.0,12.0,8.0),
                                                  child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.blueGrey.withOpacity(0.7),
                            elevation: 0.0,
                            child:MaterialButton(onPressed: ()async{
                              if(_formKeys.currentState.validate()){
                                if(!await user.signUp(_nameTextController.text,email.text, password.text)) {
                                  _key.currentState.showSnackBar(SnackBar(content:Text("Sign up failed")));
                                }
                                   Navigator.push(
                                      context, MaterialPageRoute(builder: (context)=> new ScreenController()));
                                   
                              }
                              
                            },
                            minWidth:MediaQuery.of(context).size.width,
                             child: Text('sign up',textAlign:TextAlign.center,
                             style: TextStyle(color:Colors.white,fontWeight:FontWeight.bold),
                            ),),
                         ),
                         

                      ),
                      Padding(
                        padding:EdgeInsets.all(8.0),
                        child: InkWell(
                          child: Text("login",style:TextStyle(color: Colors.red)),
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> new LoginPage()));
                          },),),
                          
                      ],
                      )
                      )  ,

                    ),
                  
                ),

              
            ],
         
      ));
  
    
    
 
 
  }
  ValueChanged(e){
  setState((){
    if(e=="male"){
      groupValue=e;
      gender=e;
    }else if(e=="female"){
      groupValue=e;
      gender=e;
    }});
  

  }
  Future validateForm()async{
    FormState formState=_formKeys.currentState;
    if(formState.validate()){
      formState.reset();
        FirebaseUser user= (await firebaseAuth.createUserWithEmailAndPassword(
           email: email.text, 
           password: password.text)).user;
           print(user.uid.toString());
            
      
          
           if(user.uid!=null)
          {
              
                _userServices.createUser(
                  {
                    "username":_nameTextController.text,
                    "email":email.text,
                    "userId":user.uid,
                    "gender":gender,
                  }
                );

           Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Intermediate()));
      


    }
  }
  
}
}
 */
