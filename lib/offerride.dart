import 'package:flutter/material.dart';
import 'offer_by_me.dart';
import 'login.dart';
import 'package:intl/intl.dart';

import "dart:convert";

import 'package:intl/intl.dart';
import 'package:sharefare/offer_by_me.dart';
import './db/formsavedb.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:datetime_picker_formfield/time_picker_formfield.dart';

class OfferRide extends StatefulWidget {
  @override
  _OfferRideState createState() => _OfferRideState();
}

class _OfferRideState extends State<OfferRide> {
  final FirebaseAuth firebaseAuth= FirebaseAuth.instance;
  int _currentStep = 0;
  String value;
  static MyData data = new MyData();
  static var _focusNode = new FocusNode();
  final dateFormat = DateFormat("yyyy-MM-dd");
  final timeFormat = DateFormat("h:mm");
  final _formKey = GlobalKey<FormState>();
  OfferServices _userServices= OfferServices();
  TextEditingController origin= TextEditingController();
  TextEditingController destination= TextEditingController();
  TextEditingController addCity= TextEditingController();
  TextEditingController dipdate= TextEditingController();
  TextEditingController diptime= TextEditingController();
  TextEditingController noofseats= TextEditingController();
  TextEditingController registrationNo  = TextEditingController();
  TextEditingController aadharcardNo = TextEditingController();

  DateTime date;
  DateTime ddd;
  String tttt;
  TimeOfDay time;
  void initState() {
   validateuser();
 }
   Future validateuser()async{
    FirebaseUser user= await firebaseAuth.currentUser(); 
    print(user.uid.toString());
    if(user==null)
    {
      Navigator.push(context, MaterialPageRoute(builder: (context)=> new LoginPage()));
    }
    else{
      print(user.uid.toString());
    }
  }
  @override
  Widget build(BuildContext context) {
           return new Scaffold(
             backgroundColor: Colors.white,
             resizeToAvoidBottomPadding: false, 
             appBar: AppBar(
              // backgroundColor: Colors.green,
               title: Text("OFFER RIDE",
               style: TextStyle(color: Colors.white,),)
               ),
            body: Form(
              key: _formKey,
                          child: new Stepper(
                type: StepperType.vertical,
                currentStep: _currentStep,
                onStepTapped: (int step) => setState(() => _currentStep = step),
                onStepContinue:
                    _currentStep < 7 ? () => setState(() => _currentStep += 1) : null,
                onStepCancel:
                    _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
                controlsBuilder: (BuildContext context,{VoidCallback onStepContinue,VoidCallback onStepCancel}) {
                  return Row(
                    children: <Widget>[
                     _currentStep == 5 // this is the last step
                ? 
                  RaisedButton.icon(
                  icon: Icon(Icons.local_taxi),
                  onPressed: (){
                         offerrideform();
                  },
                  label: Text('Offer Ride'),
                  color: Colors.cyan,
                )
                : RaisedButton.icon(
                  icon: Icon(Icons.navigate_next),
                  onPressed: onStepContinue,
                  label: Text('Next'),
                  color: Colors.grey,
                ),
                
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton.icon(
                    icon: Icon(Icons.delete_forever),
                    label: const Text('Back'),
                    color: Colors.blueGrey,
                    onPressed: onStepCancel,
                  ),
                )
                    ],
                  );
                },
                steps: <Step>[
                  new Step(
                    title: new Text('Location'),
                    content: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: origin,
                          //focusNode: _focusNode,
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onSaved: (String value) {
                            data.olocation = value;
                          },
                          maxLines: 1,
                          //initialValue: 'Aseem Wangoo',
                          validator: (value) {
                            if (value.isEmpty || value.length < 1) {
                              return 'Please enter origin';
                            }
                          },
                          decoration: InputDecoration(labelText: 'Origin'),
                        ),
                        TextFormField(
                          controller: destination,
                          focusNode: _focusNode,
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onSaved: (String value) {
                            data.dlocation = value;
                          },
                          maxLines: 1,
                          //initialValue: 'Aseem Wangoo',
                          validator: (value) {
                            if (value.isEmpty || value.length < 1) {
                              return 'Please enter destination';
                            }
                          },
                          decoration: InputDecoration(labelText: 'Destination'),
                        ),
                      ],
                    ),
                    isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 0 ? StepState.complete : StepState.disabled,
                  ),
                  new Step(
                    title: new Text('stopover'),
                    content: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: addCity,
                          //focusNode: _focusNode,
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onSaved: (String value) {
                            data.slocation = value;
                          },
                          maxLines: 1,
                          //initialValue: 'Aseem Wangoo',
                          decoration: InputDecoration(labelText: 'Add City'),
                        ),
                      ],
                    ),
                    isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 1 ? StepState.complete : StepState.disabled,
                  ),
                  new Step(
                    title: new Text('Date'),
                    content: Container(
                      height: 80,
                      width: 200,
                      child: ListView(
                        children: <Widget>[
                          Container(
                            child: DateTimePickerFormField(
                        
                              format: dateFormat,
                              inputType: InputType.date,
                              decoration: InputDecoration(labelText: 'Date'),
                              onChanged: (dt) => setState(() => date = dt
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 2 ? StepState.complete : StepState.disabled,
                  ),
                  
                
                  new Step(
                    title: new Text('Time'),
                    
                    content: Container(
                      height: 80,
                      width: 200,
                      child: ListView(
                        children: <Widget>[
                          Container(
                            child:TimePickerFormField(
                            
                              format: timeFormat,
                              decoration: InputDecoration(labelText: 'Time'),
                              onChanged: (t) => setState(() =>time = t
                              ),
                            ),
                          ),
                        ],
                      ),
                    ), 
                    isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 3 ? StepState.complete : StepState.disabled,
                  ),
                  new Step(
                    title: new Text('Seats'),
                    content: DropdownButton(
                    
                  value: value,
              isExpanded: true,
              hint: Text("Choose no. of seats"),
              items: [
                DropdownMenuItem(
                  child: Text("1"),
                  value: "1",
                ),
                DropdownMenuItem(
                  child: Text("2"),
                  value: "2",
                ),
                DropdownMenuItem(
                  child: Text("3"),
                  value: "3",
                )
              ],
              onChanged: (newValue) {
                print(newValue);
                setState(() {
                    value = newValue;
                  });
              }),
                isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 4 ? StepState.complete : StepState.disabled,  
              ),
              new Step(
                    title: new Text('Amount'),
                    content:new Text('This is the fifth step.'),
                isActive: _currentStep >= 0,
                    state:
                        _currentStep >= 5 ? StepState.complete : StepState.disabled,  
              ),

          ],
        ),
            ),
      );
  }
  dynamic myEncode(dynamic item) {
  if(item is DateTime) {
    return item.toIso8601String();
  }
  return item;
  }
  Future offerrideform()async{
    FormState formState=_formKey.currentState;
    ddd= new DateTime(date.year,date.month,date.day,time.hour,time.minute);
    String dddd= myEncode(ddd);
    print(dddd.toString());
    
    if(formState.validate()){
      formState.reset();
      print(aadharcardNo.text);
      print(registrationNo.text);
        FirebaseUser user=await firebaseAuth.currentUser();
        print(dipdate.toString());
           if(user.uid!=null)
          {
              
                _userServices.offerdRide(
                  {
                    "origin":origin.text,
                    "destination":destination.text,
                    "userId":user.uid,
                    "datetime":dddd.toString(),
                    "city":addCity.text,
                    "noofseats":value.toString(),
                  }
                );

           Navigator.push(context, MaterialPageRoute(builder: (context)=>OfferedRide()));
      
    }
  }
  
}
}


class MyData {
  String olocation = '';
  String dlocation = '';
  String slocation = '';
  String registeration = '';
}

